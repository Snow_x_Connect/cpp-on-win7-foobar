#include <iostream>
#include <cstdio>
using namespace std;

struct Node{
    Node* next;
    int value;
    Node(int s){
        this->value = s;
        this->next=NULL;
    }
};
int main()
{
    cout << "Hello world!" << endl;
    int s;
    cin>>s;
    Node *p = new Node(1);
    p->next = new Node(2);
    p->next->next = new Node(3);

    return 0;
}
